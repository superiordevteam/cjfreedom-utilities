
package me.RyanWild.CJFUtils;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class CJFreedom_Utils extends JavaPlugin
{
    public static final Logger logger = Logger.getLogger("Minecraft-Server");

    @Override
    public void onDisable()
    {
        logger.info("CJFreedomMod Utils Disabled");
    }

    @Override
    public void onEnable()
    {        
        PluginDescriptionFile pdf = this.getDescription();
        logger.log(Level.INFO, "{0} Version{1} Has Been Enabled", new Object[]{pdf.getName(), pdf.getVersion()});
        getConfig().options().copyDefaults(true);
        saveConfig();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
    {
        if (cmd.getName().equalsIgnoreCase("test"))
        {
            sender.sendMessage("test");
        }
        return false;
    }
}
